"use strict"; 

const currentPic = document.querySelector('.image-to-show');
const onBtn = document.querySelector('.on-btn');
const offBtn = document.querySelector('.off-btn');


const pics = ["./img/1.jpg", "./img/2.jpg", "./img/3.jpg", "./img/4.png"];

let index = 0;
let isClick = true;

const playPics = () => {
    if (index === pics.length) {
        index = 0;
    }
    currentPic.src = pics[index++];
}

let interval = setInterval(playPics, 3000);

offBtn.addEventListener('click', () => {
    clearInterval(interval);
    isClick = false;
})

onBtn.addEventListener('click', () => {
    if (!isClick) {
        interval = setInterval(playPics, 3000);
    }
    isClick = true;
})
